package ccf.android.base.example;

import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.ccf.android.ui.widget.BackImageView;
import com.ccf.android.ui.widget.Button;
import com.ccf.android.ui.widget.CircleImageView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BackImageView backImageView = (BackImageView) findViewById(R.id.backImageView);
                backImageView.setEnabledWithAnimation(!backImageView.isEnabled());

                Button enabledButton = (Button) findViewById(R.id.enabledButton);
                enabledButton.setEnabledWithAnimation(!enabledButton.isEnabled());
            }
        });

        final Button enabledButton = (Button) findViewById(R.id.enabledButton);
        enabledButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enabledButton.setEnabledWithAnimation(!enabledButton.isEnabled());
            }
        });

        CircleImageView circleImageView = (CircleImageView) findViewById(R.id.circleImageView);
        circleImageView.setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.MULTIPLY);
    }
}
