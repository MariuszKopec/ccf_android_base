package com.ccf.android.ui.widget;

import android.content.Context;
import android.util.AttributeSet;

import com.ccf.baseview.R;

public class ProgressView extends com.rey.material.widget.ProgressView {
    public ProgressView(Context context) {
        this(context, null);
    }

    public ProgressView(Context context, AttributeSet attrs) {
        super(context, attrs, R.attr.ccfProgressViewStyle);
    }

    public ProgressView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
