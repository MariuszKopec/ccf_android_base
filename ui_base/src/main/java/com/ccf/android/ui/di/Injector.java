package com.ccf.android.ui.di;

import dagger.ObjectGraph;

public interface Injector {
    void inject(Object object);
    ObjectGraph getObjectGraph();
}
