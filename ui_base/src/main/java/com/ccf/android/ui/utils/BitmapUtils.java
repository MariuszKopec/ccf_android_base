package com.ccf.android.ui.utils;

import android.graphics.Bitmap;

public interface BitmapUtils {
    Bitmap decodeBitmapFromBase64(Object base64);
}
