package com.ccf.android.ui.utils.impl;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import com.ccf.android.ui.utils.BitmapUtils;


public class BitmapUtilsImpl implements BitmapUtils {
    @Override
    public Bitmap decodeBitmapFromBase64(Object base64) {
        byte[] bytes = Base64.decode((String) base64, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }
}
