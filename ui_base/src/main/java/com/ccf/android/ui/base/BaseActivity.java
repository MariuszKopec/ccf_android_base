package com.ccf.android.ui.base;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.ccf.android.ui.di.Injector;

public abstract class BaseActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Injector injector = (Injector) getApplicationContext();
        injector.inject(this);
    }
}
