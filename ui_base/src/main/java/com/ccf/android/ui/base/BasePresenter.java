package com.ccf.android.ui.base;

import com.ccf.logic.interactor.UseCase;

import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;

public class BasePresenter {
    private final List<UseCase> useCases = new ArrayList<>();

    public void onDestroy() {
        unsubscribe();
    }

    protected void unsubscribe() {
        for (UseCase useCase : useCases)
            useCase.unsubscribe();
    }

    protected void registerUseCase(UseCase useCase) {
        useCases.add(useCase);
    }

    protected void executeUseCase(UseCase useCase, Subscriber subscriber) {
        useCase.execute(subscriber);
        registerUseCase(useCase);
    }
}
