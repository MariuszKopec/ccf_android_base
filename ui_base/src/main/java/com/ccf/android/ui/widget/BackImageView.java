package com.ccf.android.ui.widget;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.ccf.android.ui.animation.ColorAnimation;
import com.ccf.android.ui.animation.ColorAnimationListener;
import com.ccf.baseview.R;

public class BackImageView extends ImageView {
    public BackImageView(Context context) {
        super(context);
        setDefaultImage();
    }

    public BackImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setDefaultImage();
    }

    public BackImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setDefaultImage();
    }

    private void setDefaultImage() {
        Drawable back_image = ContextCompat.getDrawable(getContext(), R.drawable.ic_navigate_before_black).mutate();
        back_image.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorAccent), PorterDuff.Mode.MULTIPLY);
        setImageDrawable(back_image);
    }

    public void setEnabledWithAnimation(final boolean enabled) {
        super.setEnabled(enabled);

        int colorEnabled = ContextCompat.getColor(getContext(), R.color.colorAccent);
        int colorDisabled = ContextCompat.getColor(getContext(), R.color.colorDisabled);
        int colorFrom = enabled ? colorDisabled : colorEnabled;
        int colorTo = enabled ? colorEnabled : colorDisabled;
        int duration = getResources().getInteger(android.R.integer.config_shortAnimTime);
        ColorAnimation colorAnimation = new ColorAnimation(colorFrom, colorTo, duration);
        colorAnimation.start(new ColorAnimationListener() {
            @Override
            public void onAnimationColorUpdate(int color) {
                Drawable back_image = ContextCompat.getDrawable(getContext(), R.drawable.ic_navigate_before_black).mutate();
                back_image.setColorFilter(color, PorterDuff.Mode.MULTIPLY);
                setImageDrawable(back_image);
            }
        });
    }
}
