package com.ccf.android.ui.animation;

public interface ColorAnimationListener {
    void onAnimationColorUpdate(int color);
}
