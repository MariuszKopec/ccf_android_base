package com.ccf.android.ui.widget;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatEditText;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.ccf.baseview.R;

public class PasswordEditText extends AppCompatEditText {
    private Drawable eye;
    private Drawable eyeWithStrike;
    private boolean visible = false;

    public PasswordEditText(Context context) {
        super(context);
        init(null);
    }

    public PasswordEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public PasswordEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        eye = ContextCompat.getDrawable(getContext(), R.drawable.ic_eye).mutate();
        eye.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorSecondaryIcon), PorterDuff.Mode.MULTIPLY);
        eyeWithStrike = ContextCompat.getDrawable(getContext(), R.drawable.ic_eye_strike).mutate();
        eyeWithStrike.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorSecondaryIcon), PorterDuff.Mode.MULTIPLY);
        setup();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP
                && event.getX() >= (getRight() - getCompoundDrawables()[2].getBounds().width())) {
            visible = !visible;
            setup();
            invalidate();
            return true;
        }

        return super.onTouchEvent(event);
    }

    protected void setup() {
        setInputType(InputType.TYPE_CLASS_TEXT | (visible ? InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD : InputType.TYPE_TEXT_VARIATION_PASSWORD));
        Drawable drawable = !visible ? eyeWithStrike : eye;
        Drawable[] drawables = getCompoundDrawables();
        setCompoundDrawablesWithIntrinsicBounds(drawables[0], drawables[1], drawable, drawables[3]);
    }

    @Override
    public void setInputType(int type) {
        super.setInputType(type);
        setTypeface(getTypeface());
    }
}
