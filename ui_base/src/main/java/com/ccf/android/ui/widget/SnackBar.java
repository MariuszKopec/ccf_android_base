package com.ccf.android.ui.widget;

import android.content.Context;
import android.util.AttributeSet;

public class SnackBar extends com.rey.material.widget.SnackBar {
    public SnackBar(Context context) {
        super(context);
    }

    public SnackBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SnackBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
