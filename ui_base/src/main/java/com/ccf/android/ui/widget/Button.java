package com.ccf.android.ui.widget;

import android.animation.Animator;
import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;

import com.ccf.android.ui.animation.ColorAnimation;
import com.ccf.android.ui.animation.ColorAnimationListener;
import com.ccf.android.ui.animation.DefaultAnimationListener;
import com.ccf.baseview.R;
import com.rey.material.drawable.RippleDrawable;

public class Button extends com.rey.material.widget.Button {

    public Button(Context context) {
        this(context, null);
    }

    public Button(Context context, AttributeSet attrs) {
        super(context, attrs, R.attr.ccfButtonStyle);
    }

    public Button(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setEnabledWithAnimation(final boolean enabled) {
        int colorEnabled = ContextCompat.getColor(getContext(), R.color.colorAccent);
        int colorDisabled = ContextCompat.getColor(getContext(), R.color.colorDisabled);
        int colorFrom = enabled ? colorDisabled : colorEnabled;
        int colorTo = enabled ? colorEnabled : colorDisabled;
        int duration = getResources().getInteger(android.R.integer.config_shortAnimTime);
        ColorAnimation colorAnimation = new ColorAnimation(colorFrom, colorTo, duration);
        colorAnimation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                setEnabled(enabled);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
        colorAnimation.start(new ColorAnimationListener() {
            @Override
            public void onAnimationColorUpdate(int color) {
                RippleDrawable background = (RippleDrawable) getBackground();
                background.setBackgroundDrawable(new ColorDrawable(color));
            }
        });
    }



    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(isEnabled())
            return super.onTouchEvent(event);
        return false;
    }
}
