package com.ccf.android.ui.animation;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.graphics.Color;

public class ColorAnimation {
    private int colorFrom;
    private int colorTo;
    private int duration;
    private Animator.AnimatorListener listener;

    public ColorAnimation(int colorFrom, int colorTo, int duration) {
        this.colorFrom = colorFrom;
        this.colorTo = colorTo;
        this.duration = duration;
    }

    public void addListener(Animator.AnimatorListener listener) {
        this.listener = listener;
    }

    public void start(final ColorAnimationListener delegate) {
        ValueAnimator anim = ValueAnimator.ofFloat(0, 1);
        if(listener != null)
            anim.addListener(listener);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float position = animation.getAnimatedFraction();
                int blendedColor = blendColors(position);
                delegate.onAnimationColorUpdate(blendedColor);
            }
        });
        anim.setDuration(duration).start();
    }

    private int blendColors(float ratio) {
        final float inverseRatio = 1f - ratio;

        final float r = Color.red(colorTo) * ratio + Color.red(colorFrom) * inverseRatio;
        final float g = Color.green(colorTo) * ratio + Color.green(colorFrom) * inverseRatio;
        final float b = Color.blue(colorTo) * ratio + Color.blue(colorFrom) * inverseRatio;

        return Color.rgb((int) r, (int) g, (int) b);
    }
}
