package com.ccf.android.ui.animation;

import android.view.animation.Animation;

public class DefaultAnimationListener implements Animation.AnimationListener {
    @Override
    public void onAnimationStart(Animation animation) {
    }

    @Override
    public void onAnimationEnd(Animation animation) {
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
    }
}

