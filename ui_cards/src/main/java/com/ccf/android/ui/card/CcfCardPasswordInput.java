package com.ccf.android.ui.card;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ccf.android.ui.R;
import com.ccf.android.ui.animation.ColorAnimation;
import com.ccf.android.ui.animation.ColorAnimationListener;
import com.ccf.android.ui.widget.PasswordEditText;

public class CcfCardPasswordInput implements CcfCard {
    private FragmentActivity activity;
    private final int cardType;
    private final int icon;
    private final String hint;
    private TextWatcher textWatcher;
    private InnerViewHolder viewHolder;

    public CcfCardPasswordInput(int cardType, int icon, String hint) {
        this.cardType = cardType;
        this.icon = icon;
        this.hint = hint;
    }

    @Override
    public void setViewHolder(RecyclerView.ViewHolder viewHolder) {
        this.viewHolder = (InnerViewHolder) viewHolder;
        this.viewHolder.textInputLayout.setHint(hint);
        this.viewHolder.setFocus(false);
        if(textWatcher != null) {
            this.viewHolder.editText.removeTextChangedListener(textWatcher);
            this.viewHolder.editText.addTextChangedListener(textWatcher);
        }
    }

    @Override
    public int getCardType() {
        return cardType;
    }

    @Override
    public RecyclerView.ViewHolder getViewHolder(ViewGroup parent) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.ccf_card_password_input, parent, false);
        return new InnerViewHolder(v);
    }

    @Override
    public void setActivity(FragmentActivity activity) {
        this.activity = activity;
    }

    public void setTextWatcher(TextWatcher textWatcher) {
        this.textWatcher = textWatcher;
    }

    public String getText() {
        return viewHolder.editText.getText().toString();
    }

    public void setError(String error) {
        viewHolder.textInputLayout.setError(error);
    }

    private class InnerViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        PasswordEditText editText;
        TextInputLayout textInputLayout;

        public InnerViewHolder(View v) {
            super(v);
            imageView = (ImageView) v.findViewById(R.id.ccf_cards_input_password_icon);
            editText = (PasswordEditText) v.findViewById(R.id.ccf_cards_input_password_edit_text);
            textInputLayout = (TextInputLayout) v.findViewById(R.id.ccf_cards_input_password_edit_layout);
            editText.setTag(cardType);
            editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    setFocus(hasFocus);
                }
            });
        }

        void setFocus(boolean focused) {
            int colorEnabled = ContextCompat.getColor(activity, R.color.colorAccent);
            int colorDisabled = ContextCompat.getColor(activity, R.color.colorPrimaryIcon);
            int colorFrom = focused?colorDisabled:colorEnabled;
            int colorTo = focused?colorEnabled:colorDisabled;
            int duration = activity.getResources().getInteger(android.R.integer.config_shortAnimTime);
            ColorAnimation colorAnimation = new ColorAnimation(colorFrom, colorTo, duration);
            colorAnimation.start(new ColorAnimationListener() {
                public void onAnimationColorUpdate(int color) {
                    Drawable back_image = ContextCompat.getDrawable(activity, icon).mutate();
                    back_image.setColorFilter(color, PorterDuff.Mode.MULTIPLY);
                    imageView.setImageDrawable(back_image);
                }
            });
        }
    }
}
