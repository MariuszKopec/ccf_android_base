package com.ccf.android.ui.list;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.ccf.android.ui.card.CcfCard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CcfCardAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final List<CcfCard> cards = new ArrayList<>();
    private final Map<Integer, CcfCard> useCardForViewType = new HashMap<>();
    private final FragmentActivity activity;

    public CcfCardAdapter(FragmentActivity activity) {
        this.activity = activity;
    }

    @Override
    public int getItemViewType(int position) {
        return cards.get(position).getCardType();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CcfCard card = useCardForViewType.get(viewType);
        return card.getViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        CcfCard card = cards.get(position);
        card.setActivity(activity);
        card.setViewHolder(holder);
    }

    @Override
    public int getItemCount() {
        return cards.size();
    }

    public void addCard(CcfCard card) {
        cards.add(card);
        useCardForViewType.put(card.getCardType(), card);
    }

    public void removeCard(CcfCard card) {
        cards.remove(card);
        useCardForViewType.remove(card.getCardType());
    }

    public void replaceCard(CcfCard oldCard, CcfCard newCard) {
        int position=-1;
        for (int i=0; i<cards.size(); i++)
            if(cards.get(i) == oldCard)
                position = i;
        cards.set(position, newCard);
        useCardForViewType.remove(oldCard.getCardType());
        useCardForViewType.put(newCard.getCardType(), newCard);
    }
}
