package com.ccf.android.ui.presenter;

import com.ccf.android.ui.base.BasePresenter;
import com.ccf.android.ui.card.CcfCard;

import java.util.List;

public class CcfCardListPresenterImpl extends BasePresenter implements CcfCardListPresenter {
    protected CcfCardListPresenter.CardListView view;
    protected List<CcfCard> ccfCards;

    public CcfCardListPresenterImpl(List<CcfCard> ccfCards) {
        this.ccfCards = ccfCards;
    }

    @Override
    public void setView(CardListView view) {
        this.view = view;
    }

    @Override
    public void init() {
        view.initRecyclerView();
        for (CcfCard ccfCard : ccfCards)
            view.addCard(ccfCard);
    }

    @Override
    public void addCard(CcfCard card) {
        ccfCards.add(card);
        view.addCard(card);
    }

    @Override
    public void removeCard(CcfCard card) {
        ccfCards.remove(card);
        view.removeCard(card);
    }

    @Override
    public void replaceCard(CcfCard oldCard, CcfCard newCard) {
        for(int i=0; i<ccfCards.size(); i++)
            if(ccfCards.get(i) == oldCard)
                ccfCards.set(i, newCard);
        view.replaceCard(oldCard, newCard);
    }
}
