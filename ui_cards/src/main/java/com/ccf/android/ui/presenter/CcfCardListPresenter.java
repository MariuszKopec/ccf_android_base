package com.ccf.android.ui.presenter;

import com.ccf.android.ui.card.CcfCard;

public interface CcfCardListPresenter {

    void setView(CardListView view);
    void init();
    void addCard(CcfCard card);
    void removeCard(CcfCard card);
    void replaceCard(CcfCard oldCard, CcfCard newCard);

    interface CardListView {
        void initRecyclerView();
        void addCard(CcfCard any);
        void removeCard(CcfCard card);
        void replaceCard(CcfCard oldCard, CcfCard newCard);
    }
}
