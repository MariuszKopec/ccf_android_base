package com.ccf.android.ui.list;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.ccf.android.ui.base.BaseFragment;
import com.ccf.android.ui.card.CcfCard;
import com.ccf.android.ui.presenter.CcfCardListPresenter;

import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

@EFragment(resName = "ccf_cards_fragment")
public abstract class CcfCardListFragment extends BaseFragment implements CcfCardListPresenter.CardListView {
    @ViewById
    public RecyclerView recyclerView;
    public RecyclerView.LayoutManager layoutManager;
    public CcfCardAdapter adapter;

    @Override
    public void initRecyclerView() {
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this.getActivity());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new CcfCardAdapter(this.getActivity());
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void addCard(CcfCard card) {
        adapter.addCard(card);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void removeCard(CcfCard card) {
        adapter.removeCard(card);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void replaceCard(CcfCard oldCard, CcfCard newCard) {
        adapter.replaceCard(oldCard, newCard);
        adapter.notifyDataSetChanged();
    }
}
