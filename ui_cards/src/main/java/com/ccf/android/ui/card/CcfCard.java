package com.ccf.android.ui.card;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

public interface CcfCard {
    void setViewHolder(RecyclerView.ViewHolder viewHolder);
    int getCardType();
    RecyclerView.ViewHolder getViewHolder(ViewGroup parent);
    void setActivity(FragmentActivity activity);
}
